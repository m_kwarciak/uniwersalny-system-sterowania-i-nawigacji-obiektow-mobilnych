package com.mateusz.nawigacja.serwer.GUI;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTreeTableView;
import com.mateusz.nawigacja.serwer.Communication.Communicator;
import com.mateusz.nawigacja.serwer.Interfaces.PositionFilterInterface;
import com.mateusz.nawigacja.serwer.Interfaces.VehicleInterface;
import com.mateusz.nawigacja.serwer.Navigation.AvailableController;
import com.mateusz.nawigacja.serwer.Navigation.Checkpoint;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.util.Callback;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

/**
 * Created by Mateusz on 25.02.2018.
 */
public class DrawerController implements Initializable {

    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    @FXML
    private JFXButton clearButton;
    @FXML
    private JFXButton exitButton;
    @FXML
    private ComboBox<String> vehicleListComboBox;
    @FXML
    private ImageView refreshButton;
    @FXML
    private JFXButton startButton;
    @FXML
    private JFXButton stopButton;
    @FXML
    private TableView<Checkpoint> routeTableView;
    @FXML
    private JFXTreeTableView<?> routeTable;
    @FXML
    private TableColumn<Checkpoint, Integer> lpColumn;
    @FXML
    private TableColumn<Checkpoint, Double> latitudeColumn;
    @FXML
    private TableColumn<Checkpoint, Double> longitudeColumn;
    @FXML
    private TableColumn<Checkpoint, ImageView> statusColumn;

    private Timeline autoRefreshTimeline;
    private ObservableList<String> vehiclesList;
    private ObservableList<Checkpoint> observableRoute;
    private ArrayList<AvailableController> availableControllers = new ArrayList<>();
    Communicator communicator = Communicator.getInstance();
    ConcurrentHashMap<String, LocalTime> reportedVehicles;

    @FXML
    private GoogleMapViewController GMVC;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        reportedVehicles = new  ConcurrentHashMap<>();
        try {
            Thread.sleep(500);
            reportedVehicles = communicator.getReportedVehicles();
        } catch (NullPointerException | InterruptedException e) {
            GMVC.showInfoDialog("Bląd", "Nie udało się nawiązać połączenia z transmiterem. Sprawdź urządzenie a następnie zresetuj program.");
            LOGGER.severe("Nie udało się nawiązać połączenia z transmiterem. Sprawdź urządzenie a następnie zresetuj program.");
        }
        //Wczytywanie dostepnych sterowników pojazdów
        findControllers();

        //Inicjalizacja tablicy wyświetlającej szczegóły trasy
        lpColumn.setCellValueFactory(new PropertyValueFactory<>("index"));
        latitudeColumn.setCellValueFactory(new PropertyValueFactory<>("latitude"));
        longitudeColumn.setCellValueFactory(new PropertyValueFactory<>("longitude"));
        statusColumn.setCellValueFactory(new PropertyValueFactory<>("imageView"));
        routeTableView.setItems(GMVC.getRouteToExport());

        //Inicjalizacja listy dostępnych pojazdów
        for (AvailableController el : availableControllers) {
            vehicleListComboBox.getItems().add(el.getController().getRobotName());
        }
        refreshComponentVehicleListComboBox();

        refreshButton.setOnMouseClicked(e -> {
            findControllers();
            whichVehiclesAvailable();
            refreshComponentVehicleListComboBox();
        });

        //Automatyczne odswieżanie listy dostępnych pojazdów co 3 sekundy + Wyswietlanie statusu polaczenia z wybranym pojazdem
        KeyFrame autoRefresh = new KeyFrame(javafx.util.Duration.seconds(3.0), event -> {
            findControllers();
            whichVehiclesAvailable();
            refreshComponentVehicleListComboBox();
        });

        autoRefreshTimeline = new Timeline(autoRefresh);
        autoRefreshTimeline.setCycleCount(Animation.INDEFINITE);
        autoRefreshTimeline.play();
    }

    private void findControllers() {
        File directory = new File("drivers/");
        for (File jar : directory.listFiles()) {
            try {
                ClassLoader loader = URLClassLoader.newInstance(
                        new URL[]{jar.toURI().toURL()}
                );

                Class controllerClass = Class.forName("Controller", true, loader);
                VehicleInterface controller = (VehicleInterface) controllerClass.newInstance();

                Class positionFilterClass = Class.forName("PositionFilter", true, loader);
                PositionFilterInterface positionFilter = (PositionFilterInterface) positionFilterClass.newInstance();

                if (!isItDuplicate(controller.getRobotName())) {
                    availableControllers.add(new AvailableController(controller, positionFilter));
                }

            } catch (Exception e) {
                e.printStackTrace();
                LOGGER.warning("Błąd. Nie udało się poprawnie wczytać sterownika z pliku " + jar.getName());
            }
        }

    }

    private boolean isItDuplicate(String robotName) {
        for (AvailableController el: availableControllers) {
            if(el.getController().getRobotName().equals(robotName)) {
                return true;
            }
        }
        return false;
    }

    private void whichVehiclesAvailable() {
        for (AvailableController el : availableControllers) {
            if (reportedVehicles.containsKey(el.getController().getRobotName())) {
                LocalTime notificationTime = reportedVehicles.get(el.getController().getRobotName());
                LocalTime timeNow = LocalTime.now();
                long timeBetween = Duration.between(notificationTime, timeNow).toMillis();

                if (timeBetween < 5001) {
                    el.setVehicleAvailable(true);
                }
            }
        }
    }

    @FXML
    void clearButtonAction(ActionEvent event) {
        GMVC.confirmDelete();
    }

    @FXML
    void connectAction(ActionEvent event) {
        int selected = vehicleListComboBox.getSelectionModel().getSelectedIndex();
        if (selected < 0) {
            GMVC.showInfoDialog("Błąd połączenia", "Nie wybrano pojazdu");
            LOGGER.info("Błąd - Próba nawiązania połączenia z pojazdem - nie wybrano pojazdu");
            return;
        } else {
            if (!availableControllers.get(selected).isVehicleAvailable()) {
                GMVC.showInfoDialog("Błąd połączenia", "Wybrany pojazd jest obecnie niedostępny");
                LOGGER.info("Błąd - Próba nawiązania połączenia z pojazdem - Wybrany pojazd jest obecnie niedostępny");
                return;
            }
        }

        GMVC.connectToRobot(availableControllers.get(selected));
    }

    @FXML
    void exitButtonAction(ActionEvent event) {
        if (GMVC != null) {
            GMVC.exit();
        }
        System.exit(0);
    }

    @FXML
    void startButtonAction(ActionEvent event) {

        int selected = vehicleListComboBox.getSelectionModel().getSelectedIndex();
        if (selected < 0) {
            GMVC.showInfoDialog("Błąd", "Nie wybrano pojazdu");
            LOGGER.info("StartAction - Nie wybrano pojazdu");
            return;
        } else {
            if (!availableControllers.get(selected).isVehicleAvailable()) {
                GMVC.showInfoDialog("Błąd", "Wybrany pojazd jest obecnie niedostępny");
                LOGGER.info("StartAction - Wybrany pojazd jest obecnie niedostępny");
                return;
            }
        }

        GMVC.runNavigationProcess();
    }

    @FXML
    void stopButtonAction(ActionEvent event) {
        if (GMVC.autoRefreshMapTimeline != null) {
            if (GMVC.autoRefreshMapTimeline.getStatus().equals(Animation.Status.PAUSED)) {
                GMVC.autoRefreshMapTimeline.play();
            } else if (GMVC.autoRefreshMapTimeline.getStatus().equals(Animation.Status.RUNNING)) {
                GMVC.autoRefreshMapTimeline.pause();
            }
            GMVC.holdResumeNavigation();
        }
    }

    public void setGMVC(GoogleMapViewController GMVC) {
        this.GMVC = GMVC;
    }

    private void refreshComponentVehicleListComboBox() {

        vehicleListComboBox.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {

            @Override
            public ListCell<String> call(ListView<String> param) {
                final ListCell<String> cell = new ListCell<String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            setText(item);

                            boolean flag = false;
                            for (AvailableController el : availableControllers) {
                                if (item.contains(el.getController().getRobotName())) {
                                    if (el.isVehicleAvailable()) {
                                        flag = true;
                                        break;
                                    }
                                }
                            }

                            if (flag) {
                                setTextFill(Color.BLACK);
                            } else {
                                setTextFill(Color.GRAY);
                            }
                        } else {
                            setText(null);
                        }
                    }
                };
                return cell;
            }
        });
    }
}
