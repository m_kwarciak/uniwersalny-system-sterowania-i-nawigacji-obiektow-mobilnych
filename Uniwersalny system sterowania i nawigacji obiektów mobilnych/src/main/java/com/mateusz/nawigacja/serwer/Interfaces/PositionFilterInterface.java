package com.mateusz.nawigacja.serwer.Interfaces;

import com.mateusz.nawigacja.serwer.Navigation.VehiclePosition;

/**
 * Created by Mateusz on 09.05.2018.
 */
public interface PositionFilterInterface {

    /**
     * Filtruje otrzymane dane i zwraca możliwie najdokładniejszą pozycję pojazdu.
     * <p>
     * W tej metodzie powinny znaleźć się wszystkie operację dokonujące korekcji i oczyszczenia współrzędnych z zakłóceń
     *
     * @param currentPosition dane o pozycji pojazdu pochodzące z GPS
     * @return dane o pozycji pojazdu poddane obróbce i odszumowaniu
     */
    VehiclePosition filter(VehiclePosition currentPosition);

}
