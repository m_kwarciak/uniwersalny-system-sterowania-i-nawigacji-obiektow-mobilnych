package com.mateusz.nawigacja.serwer.Navigation;

/**
 * Created by Mateusz on 15.03.2018.
 */
public class VehiclePosition {

    /**
     * Szerokość geograficzna w stopniach dziesiętnych
     */
    public double latitude;

    /**
     * Długość geograficzna w stopniach dziesiętnych
     */
    public double longitude;

    /**
     * Określa wysokość nad poziomem morza w metrach
     */
    public double altitude;

    public VehiclePosition() {
    }

    public VehiclePosition(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public VehiclePosition(VehiclePosition position) {
        this.latitude = position.latitude;
        this.longitude = position.longitude;
        this.altitude = position.altitude;
    }
}
