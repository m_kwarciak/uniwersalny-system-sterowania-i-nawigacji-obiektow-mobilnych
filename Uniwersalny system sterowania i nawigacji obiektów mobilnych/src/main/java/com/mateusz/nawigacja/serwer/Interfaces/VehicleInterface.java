package com.mateusz.nawigacja.serwer.Interfaces;

import com.mateusz.nawigacja.serwer.Navigation.VehiclePosition;

/**
 * Created by Mateusz on 15.03.2018.
 */
public interface VehicleInterface {

    /**
     * Zwraca unikalną nazwę pojazdu, taką samą jak ta za pomocą której pojazd zgłasza swoją gotowość. Należy zadbać
     * by nazwa pojazdu była unikalna, nie pokrywała się z nazwami pozostałych urządzeń działających w systemie.
     * Nazwa powinna składać się z liter. Zabronione jest używanie znaku "@"
     *
     * @return
     */
    String getRobotName();

    /**
     * Zwraca aktualne dane o pozycji robota umieszczone w obiekcie klasy VehiclePosition.
     * <p>
     *
     * Metoda otrzymuję argument w postaci "nazwa_pojazdu@informacje_z_pojazdu"
     * <p>
     * Metoda powinna sparsować otrzymaną wiadomość, wydobyć z niej informację o
     * lokalizacji pojazdu oraz zapisać wydobyte dane w obiekcie klasy VehiclePosition
     * @param messageFromVehicle ramka z danymi wysłanymi z pojazdu
     * @return Aktualne dane o pojeździe
     */
    VehiclePosition getVehiclePosition(String messageFromVehicle);

    /**
     * Metoda zwraca ramkę zawierającą informację zrozumiałą dla pojazdu.
     * Po otrzymaniu tej informacji pojazd powinien móc wykonać sekwencję czynności prowadzącą do
     * realizacji zadania skrętu o podany kąt
     * <p>
     * Metoda zwraca samą ramkę z informacjami dla pojazdu. Nie opatruje jej adnotacjami typu "nazwa_pojazdu@", ani
     * nie przesyła żadnych informacji do pojazdu. Realizacją przesłania ramki zajmuje się klasa Navigator
     *
     * @param degree   Pod jakim kątem, względem pojazdu, znajduje się obecny cel
     * @param distance O ile metrów od pojazdu, oddalony jest obecny cel
     */
    String goToNextPoint(double degree, double distance);

    /**
     * Metoda zatrzymująca pojazd
     * Po otrzymaniu tej informacji pojazd powinien stać w miejscu
     * @return
     */
    String stop();

}
