package com.mateusz.nawigacja.serwer;

import com.mateusz.nawigacja.serwer.Log.MyLogger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by Mateusz on 24.02.2018.
 */
public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage primaryStage) throws Exception {

        MyLogger.setup();

        Parent root = FXMLLoader.load(getClass().getResource("/com.mateusz.nawigacja.GUI/Scene.fxml"));
        Scene scene = new Scene(root);

        primaryStage.setTitle("Uniwersalny system sterowania i nawigacji obiektów mobilnych");
        primaryStage.setMinWidth(1024);
        primaryStage.setMinHeight(768);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
