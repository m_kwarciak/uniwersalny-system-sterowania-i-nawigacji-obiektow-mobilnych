package com.mateusz.nawigacja.serwer.Navigation;

/**
 * Created by Mateusz on 31.05.2018.
 */
public class Checkpoint extends VehiclePosition{

    private int index;
    private Boolean done;

    public Checkpoint(double latitude, double longitude) {
        super(latitude, longitude);
        this.done = Boolean.FALSE;
    }

    public Checkpoint(double latitude, double longitude, int index) {
        this(latitude, longitude);
        this.index = index;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public int getIndex() {
        return index;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(){
        this.done = true;
    }

}
