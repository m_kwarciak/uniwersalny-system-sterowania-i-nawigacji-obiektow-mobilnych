package com.mateusz.nawigacja.serwer.Communication;

import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Logger;

/**
 * Created by Mateusz on 19.05.2018.
 */
public class SerialPortWriter {

    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private OutputStream outputStream;

    public SerialPortWriter(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    public boolean send(String message) {
        try {
            outputStream.write(message.getBytes());
            outputStream.flush();
            LOGGER.info("Wysłano wiadomość: " + message);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
