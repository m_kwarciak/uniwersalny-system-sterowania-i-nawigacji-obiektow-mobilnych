package com.mateusz.nawigacja.serwer.Communication;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import java.io.*;
import java.time.LocalTime;
import java.util.Enumeration;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;


/**
 * Klasa służąca do odbierania i wysyłania danych z i do pojazdu
 * Created by Mateusz on 15.03.2018.
 */
public class Communicator {

    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private static volatile Communicator instance;

    private InputStream inputStream;
    private OutputStream outputStream;

    private SerialPortWriter serialPortWriter;
    private SerialPortReader serialPortReader;

    private Communicator() {
        try {
            connect();
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.warning("Nie udało się zainicjować instancji klasy COmmunicator");
        }
    }

    public static Communicator getInstance() {
        Communicator result = instance;
        if (result == null) {
            synchronized (Communicator.class) {
                if (result == null)
                    instance = new Communicator();
            }
        }
        return result;
    }

    public boolean connect() throws Exception {
        try {
            SerialPort serialPort = findSerialPort();

            if (serialPort == null) {
                LOGGER.warning("Nie wykryto transmitera danych");
                return false;
            } else {
                inputStream = serialPort.getInputStream();
                outputStream = serialPort.getOutputStream();

                serialPortReader = new SerialPortReader(inputStream, serialPort);
                serialPortWriter = new SerialPortWriter(outputStream);

                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.warning("Błąd w trakcie próby nawiązania połączenia z urządzeniem do tranmisji danych");
        }
        return false;
    }

    private SerialPort findSerialPort() {
        SerialPort sp;
        try {
            Enumeration<?> enumeration = CommPortIdentifier.getPortIdentifiers();
            while (enumeration.hasMoreElements()) {
                CommPortIdentifier commPortIdentifier = (CommPortIdentifier) enumeration.nextElement();
                if (commPortIdentifier.getPortType() == CommPortIdentifier.PORT_SERIAL) {

                    sp = (SerialPort) commPortIdentifier.open("SerialPort", 500);
                    sp.setSerialPortParams(
                            57600,
                            SerialPort.DATABITS_8,
                            SerialPort.STOPBITS_1,
                            SerialPort.PARITY_NONE
                    );

                    LOGGER.info("Połączono: " + sp.getName());
                    return sp;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.warning("Błąd w trakcie nawiązać połączenia z urządzeniem transmisyjnym");
        }
        LOGGER.warning("Nie udało się nawiązać połączenia");
        return null;
    }

    public boolean send(String robotName, String message) {
        if (serialPortWriter != null) {
            return serialPortWriter.send(robotName + "@" + message);
        } else {
            return false;
        }
    }

    public static ConcurrentHashMap<String, LocalTime> getReportedVehicles() {
        return SerialPortReader.getReportedVehicles();
    }

    public String getMessageFromVehicle(String robotName) {
        return serialPortReader.getReceivedMessagesFromVehicle().getOrDefault(robotName, "");
    }
}