package com.mateusz.nawigacja.serwer.Communication;

import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalTime;
import java.util.TooManyListenersException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

/**
 * Created by Mateusz on 19.05.2018.
 */
public class SerialPortReader implements SerialPortEventListener {

    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private InputStream inputStream;

    private static ConcurrentHashMap<String, LocalTime> reportedVehicles  = new ConcurrentHashMap<>(32, 75);
    private static ConcurrentHashMap<String, String> receivedMessagesFromVehicle = new ConcurrentHashMap<>();

    public SerialPortReader(InputStream inputStream, SerialPort serialPort) throws TooManyListenersException {
        this.inputStream = inputStream;

        serialPort.addEventListener(this);
        serialPort.notifyOnDataAvailable(true);
    }

    static ConcurrentHashMap<String, LocalTime> getReportedVehicles() {
        return reportedVehicles;
    }

    ConcurrentHashMap<String, String> getReceivedMessagesFromVehicle() {
        return receivedMessagesFromVehicle;
    }

    @Override
    public void serialEvent(SerialPortEvent serialPortEvent) {
        try {
            Thread.sleep(300);
            int available = inputStream.available();
            byte[] chunk = new byte[available];
            inputStream.read(chunk, 0, available);
            String message = new String(chunk);

            LOGGER.info("ODEBRANO KOMUNIKAT: " + message);

            String[] msgTab = message.split("@");
            String robotName = msgTab[0];
            if (robotName.length() == 0) {
                return;
            }
            LocalTime time = LocalTime.now();
            reportedVehicles.put( robotName, time);
            LOGGER.finest(robotName + " - Odebrano sygnał od pojazdu. Pojazd oznaczony jak aktywny");

            if (robotName.length() < message.length()) {
                receivedMessagesFromVehicle.put(robotName, message);
            }

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

}
