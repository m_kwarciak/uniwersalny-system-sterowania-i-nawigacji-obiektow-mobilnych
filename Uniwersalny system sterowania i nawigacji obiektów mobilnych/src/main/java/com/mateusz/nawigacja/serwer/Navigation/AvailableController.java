package com.mateusz.nawigacja.serwer.Navigation;

import com.mateusz.nawigacja.serwer.Interfaces.PositionFilterInterface;
import com.mateusz.nawigacja.serwer.Interfaces.VehicleInterface;

/**
 * Created by Mateusz on 03.06.2018.
 */
public class AvailableController {

    private VehicleInterface controller;
    private PositionFilterInterface positionFilter;
    private boolean isVehicleAvailable;

    public AvailableController(VehicleInterface controller, PositionFilterInterface positionFilter) {
        this.controller = controller;
        this.positionFilter = positionFilter;
    }

    public VehicleInterface getController() {
        return controller;
    }

    public PositionFilterInterface getPositionFilter() {
        return positionFilter;
    }

    public boolean isVehicleAvailable() {
        return isVehicleAvailable;
    }

    public void setVehicleAvailable(boolean vehicleAvailable) {
        isVehicleAvailable = vehicleAvailable;
    }
}
