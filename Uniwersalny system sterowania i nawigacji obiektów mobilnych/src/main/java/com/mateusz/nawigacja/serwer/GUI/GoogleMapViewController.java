package com.mateusz.nawigacja.serwer.GUI;

import com.jfoenix.controls.*;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.event.GMapMouseEvent;
import com.lynden.gmapsfx.javascript.event.UIEventType;
import com.lynden.gmapsfx.javascript.object.*;
import com.lynden.gmapsfx.shapes.Polyline;
import com.lynden.gmapsfx.shapes.PolylineOptions;
import com.mateusz.nawigacja.serwer.Navigation.AvailableController;
import com.mateusz.nawigacja.serwer.Navigation.Checkpoint;
import com.mateusz.nawigacja.serwer.Navigation.Navigator;
import com.mateusz.nawigacja.serwer.Navigation.VehiclePosition;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Logger;


/**
 * Created by Mateusz on 24.02.2018.
 */
public class GoogleMapViewController implements Initializable, MapComponentInitializedListener {

    private static final String GOOGLE_MAP_API_KEY = "WPISZ KLUCZ";

    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private DecimalFormat formatter = new DecimalFormat("###.000000");
    private GoogleMap map;
    private Polyline polyRoute;
    private Polyline polyRobotPosition;

    private ArrayList<LatLong> route = new ArrayList<>();
    private ObservableList<Checkpoint> routeToExport = FXCollections.observableArrayList();

    private ArrayList<LatLong> robotPosition = new ArrayList<>();
    private ArrayList<LatLong> redoHistory = new ArrayList<>();
    Timeline autoRefreshMapTimeline;
    private Navigator navigator;

    @FXML
    private StackPane dialogStackPane;
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private Pane coordinatesPane;
    @FXML
    private GoogleMapView mapView;
    @FXML
    private JFXDrawer drawer;
    @FXML
    private JFXHamburger hamburger;
    @FXML
    private Label isConnectedLabel;
    @FXML
    private ImageView redoButton;
    @FXML
    private ImageView undoButton;
    @FXML
    private ImageView infoButton;
    @FXML
    private ImageView addPointButton;
    @FXML
    private ImageView goToButton;
    @FXML
    private JFXTextField latitudeTextField;
    @FXML
    private JFXTextField longitudeTextField;

    public void mapInitialized() {
        MapOptions options = new MapOptions();

        options.center(new LatLong(50.067314, 19.917575)) //agh
                .zoomControl(true)
                .zoom(19)
                .overviewMapControl(false)
                .streetViewControl(false)
                .mapType(MapTypeIdEnum.ROADMAP);

        mapView.setKey(GOOGLE_MAP_API_KEY);
        map = mapView.createMap(options);

        map.addMouseEventHandler(UIEventType.click, (GMapMouseEvent event) -> {
            LatLong latLong = event.getLatLong();
            latitudeTextField.setText(formatter.format(latLong.getLatitude()));
            longitudeTextField.setText(formatter.format(latLong.getLongitude()));
        });

        map.addMouseEventHandler(UIEventType.rightclick, (GMapMouseEvent event) -> {
            LatLong latLong = event.getLatLong();
            latitudeTextField.setText(formatter.format(latLong.getLatitude()));
            longitudeTextField.setText(formatter.format(latLong.getLongitude()));
            addPointAction();
        });

        LOGGER.finest("Zainicjowano mapę");
    }

    public void initialize(URL location, ResourceBundle resources) {
        mapView.addMapInializedListener(this);

        anchorPane.widthProperty().addListener((observable, oldValue, newValue) ->
                coordinatesPane.setLayoutX(((double) newValue) / 2.0 - 180));

        infoButton.setOnMousePressed(event -> showInfoDialog(
                "Informacje", "SKN \"Creative\" \n" +
                "\n" +
                "Opiekun projektu:" +
                "\n   dr. inż. Andrzej Opaliński" +
                "\n\n" +
                "Autor" +
                "\n   Mateusz Kwarciak"));

        redoButton.setOnMousePressed(event -> redoAction());
        undoButton.setOnMousePressed(event -> undoAction());
        addPointButton.setOnMousePressed(event -> addPointAction());
        goToButton.setOnMousePressed(event -> goToAction());

        try {
            DrawerController drawerController = new DrawerController();
            drawerController.setGMVC(this);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/com.mateusz.nawigacja.gui/Drawer.fxml"));
            loader.setController(drawerController);
            AnchorPane drawerPane = (AnchorPane) loader.load();
            drawer.setSidePane(drawerPane);
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.severe("Nie udało się poprawnie zainicjować DrawerControllera. Wyjście z programu");
            System.exit(1);
        }

        HamburgerBackArrowBasicTransition burgerTask = new HamburgerBackArrowBasicTransition(hamburger);
        burgerTask.setRate(-1);
        hamburger.addEventHandler(MouseEvent.MOUSE_PRESSED, (e) -> {
            burgerTask.setRate(burgerTask.getRate() * -1);
            burgerTask.play();

            if (drawer.isShown()) {
                drawer.close();
                drawer.setDisable(true);
            } else {
                drawer.setDisable(false);
                drawer.open();
            }
        });
    }

    void showInfoDialog(String title, String message) {
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setHeading(new Text(title));
        dialogLayout.setBody(new Text(message));

        dialogStackPane.setDisable(false);
        JFXDialog confirmDeleteDialog = new JFXDialog(dialogStackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        JFXButton buttonExit = new JFXButton("Ok");
        buttonExit.addEventHandler(MouseEvent.MOUSE_CLICKED, (e) -> {
            dialogStackPane.setDisable(true);
            confirmDeleteDialog.close();
        });

        dialogLayout.setActions(buttonExit);
        confirmDeleteDialog.show();
    }

    void connectToRobot(AvailableController availableController) {
        navigator = new Navigator(availableController.getController(), availableController.getPositionFilter());

        KeyFrame update = new KeyFrame(Duration.seconds(1.0), event -> {
            robotPosition.clear();
            try {
                for (VehiclePosition el : navigator.getListOfReceivedVehiclePositions()) {
                    LatLong tmp = new LatLong(el.latitude, el.longitude);
                    robotPosition.add(tmp);
                }

                drawRobotPosition();
                if (navigator.isConnected.get()){
                    isConnectedLabel.setText("Połączono");
                } else {
                    isConnectedLabel.setText("");
                }
            } catch (Exception e) {
            }
        });

        autoRefreshMapTimeline = new Timeline(update);
        autoRefreshMapTimeline.setCycleCount(Timeline.INDEFINITE);
        autoRefreshMapTimeline.play();

        Thread thread = new Thread(navigator);
        thread.setDaemon(true);
        thread.start();

        LOGGER.info("Ustanowiono połączenie z pojazdem");
    }

    void runNavigationProcess() {
        clearRobotPositionOnMap();

        if (navigator.isConnected.get()) {
            ArrayList<Checkpoint> routeToNavigator = new ArrayList<>();

            int i = 1;
            for (LatLong el : route) {
                routeToNavigator.add(new Checkpoint(el.getLatitude(), el.getLongitude(), i));
                i++;
            }
            navigator.runNavigationProcess(routeToNavigator);
        } else {
            showInfoDialog("Blad", "Nie nawiazano jeszcze polaczenia z pojazdem");
        }
    }

    void holdResumeNavigation() {
        if (navigator != null && navigator.isConnected.get()) {
            if (navigator.runNavigation.get()) {
                navigator.runNavigation.set(false);
            } else {
                navigator.runNavigation.set(true);
            }
        }
    }

    void exit() {
        if (navigator != null && navigator.isConnected.get()) {
            if (navigator.runNavigation.get()) {
                navigator.runNavigation.set(false);
            }
        }
    }

    @SuppressWarnings("Duplicates")
    private void redoAction() {
        if (redoHistory.size() > 0) {
            int lastIndex = redoHistory.size() - 1;
            route.add(redoHistory.get(lastIndex));
            updateRouteToExport(route);
            redoHistory.remove(lastIndex);
            drawRoute();

        }
    }

    @SuppressWarnings("Duplicates")
    private void undoAction() {
        if (route.size() > 0) {
            int lastIndex = route.size() - 1;
            redoHistory.add(route.get(lastIndex));
            route.remove(lastIndex);
            updateRouteToExport(route);
            drawRoute();
        }
    }

    private void goToAction() {
        try {
            Number lat = formatter.parse(latitudeTextField.getText());
            Number lon = formatter.parse(longitudeTextField.getText());

            map.setCenter(new LatLong(lat.doubleValue(), lon.doubleValue()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    private void addPointAction() {
        try {
            Number lat = formatter.parse(latitudeTextField.getText());
            Number lon = formatter.parse(longitudeTextField.getText());

            route.add(new LatLong(lat.doubleValue(), lon.doubleValue()));
            updateRouteToExport(route);

            drawRoute();

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    void confirmDelete() {
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setHeading(new Text("Usuwanie trasy"));
        dialogLayout.setBody(new Text("Czy jesteś pewny, że chcesz usunąć tę trase?"));

        dialogStackPane.setDisable(false);
        JFXDialog confirmDeleteDialog = new JFXDialog(dialogStackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        JFXButton buttonYes = new JFXButton("Tak");
        buttonYes.addEventHandler(MouseEvent.MOUSE_CLICKED, (e) -> {
            dialogStackPane.setDisable(true);
            try {
                route.clear();
                updateRouteToExport(route);
                robotPosition.clear();
                redoHistory.clear();
                clearMap();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            confirmDeleteDialog.close();
        });

        JFXButton buttonNo = new JFXButton("Nie");
        buttonNo.addEventHandler(MouseEvent.MOUSE_CLICKED, (e) -> {
            dialogStackPane.setDisable(true);
            confirmDeleteDialog.close();
        });

        dialogLayout.setActions(buttonYes, buttonNo);
        confirmDeleteDialog.show();
    }

    private boolean firstUseDrawRouteFunction = true;

    private void drawRoute() {

        if (firstUseDrawRouteFunction) {
            firstUseDrawRouteFunction = false;
        } else {
            map.clearMarkers();
            map.removeMapShape(polyRoute);
        }

        LatLong[] latLongsArray = route.toArray(new LatLong[0]);
        MVCArray mvc = new MVCArray(latLongsArray);

        PolylineOptions polyOpts = new PolylineOptions()
                .path(mvc)
                .strokeColor("red")
                .strokeWeight(4);

        polyRoute = new Polyline(polyOpts);
        map.addMapShape(polyRoute);

        for (int i = 0; i < route.size(); i++) {
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(route.get(i));
            markerOptions.animation(Animation.DROP);

            if (i == 0)
                markerOptions.label("START");
            if (i != 0 && i == route.size() - 1)
                markerOptions.label("STOP");

            markerOptions.visible(true);
            Marker marker = new Marker(markerOptions);
            map.addMarker(marker);
        }

    }


    private boolean firstUseDrawRoutePositionFunction = true;

    private void drawRobotPosition() {

        if (firstUseDrawRoutePositionFunction) {
            firstUseDrawRoutePositionFunction = false;
        } else {
            map.clearMarkers();
            map.removeMapShape(polyRobotPosition);
        }

        LatLong[] latLongsArray = robotPosition.toArray(new LatLong[0]);
        MVCArray mvc = new MVCArray(latLongsArray);

        PolylineOptions polyOpts = new PolylineOptions()
                .path(mvc)
                .strokeColor("blue")
                .strokeWeight(4);

        polyRobotPosition = new Polyline(polyOpts);
        map.addMapShape(polyRobotPosition);
    }


    void clearMap() {
        if (polyRoute != null)
            map.removeMapShape(polyRoute);

        if (polyRobotPosition != null)
            map.removeMapShape(polyRobotPosition);

        map.clearMarkers();
    }

    void clearRobotPositionOnMap() {
        if (polyRobotPosition != null)
            map.removeMapShape(polyRobotPosition);
    }

    private void updateRouteToExport(ArrayList<LatLong> route) {
        routeToExport.clear();
        int i = 1;
        for (LatLong el : route) {
            routeToExport.add(new Checkpoint(el.getLatitude(), el.getLongitude(), i));
            i++;
        }
    }

    ObservableList<Checkpoint> getRouteToExport() {
        return routeToExport;
    }

}
