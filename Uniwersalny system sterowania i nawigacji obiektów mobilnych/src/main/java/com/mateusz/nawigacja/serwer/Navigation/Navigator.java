package com.mateusz.nawigacja.serwer.Navigation;

import com.mateusz.nawigacja.serwer.Communication.Communicator;
import com.mateusz.nawigacja.serwer.Interfaces.PositionFilterInterface;
import com.mateusz.nawigacja.serwer.Interfaces.VehicleInterface;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;

/**
 * Created by Mateusz on 15.03.2018.
 */
public class Navigator implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    private static int GPS_ACCURACY = 2;

    private Communicator communicator;

    private VehicleInterface controller;
    private PositionFilterInterface positionFilter;
    private String robotName;

    public AtomicBoolean isConnected = new AtomicBoolean(false);
    public AtomicBoolean runNavigation = new AtomicBoolean(false);

    private VehiclePosition currentGoal;
    private VehiclePosition previousPosition;
    private VehiclePosition currentPosition;

    /**
     * LIsta składająca się z pozycji jakie przebył robot do tej pory
     */
    private ArrayList<VehiclePosition> listOfReceivedVehiclePositions;

    /**
     * Lista składająca się z punktów trasy jakie ma pokonac robot. Ustalona ręcznie przez użytkownika
     */
    private ArrayList<Checkpoint> checkpoints;

    public Navigator(VehicleInterface vehicleController, PositionFilterInterface vehiclePositionFilter) {
        controller = vehicleController;
        positionFilter = vehiclePositionFilter;

        robotName = controller.getRobotName();
        communicator = Communicator.getInstance();
        listOfReceivedVehiclePositions = new ArrayList<>();
    }

    @Override
    public void run() {
        checkConnection();
        LOGGER.info("Uruchomiono watek do zarządzania pojazdem oraz nawiązano połączenie : " + robotName);
        try {
            while (true) {
                String receivedMessage = communicator.getMessageFromVehicle(robotName);

                previousPosition = currentPosition;
                currentPosition = controller.getVehiclePosition(receivedMessage);
                LOGGER.finest("Wspolrzedne odebrane z pojazdu: " + robotName + " : " + currentPosition.latitude + ", " + currentPosition.longitude);

                currentPosition = positionFilter.filter(currentPosition);
                LOGGER.finest("Wspolrzedne po przetworzeniu za pomoca filtru współrzędnych. Nazwa pojazdu: " + robotName + " : " + currentPosition.latitude + ", " + currentPosition.longitude);

                listOfReceivedVehiclePositions.add(currentPosition);

                if (runNavigation.get() && checkpoints != null) {
                    if (isGoalAchieved(currentPosition)) {
                        LOGGER.info("Biezacy cel spelniony :" + currentPosition.latitude + " " + currentPosition.longitude);
                        if (hasNext()) {
                            setNextGoal();
                            LOGGER.info("Ustanowiono nastepny cel: " + currentGoal.latitude + " " + currentGoal.longitude);
                        } else {
                            LOGGER.info("Brak następnych celów. Zakończono proces nawigacji");
                            communicator.send(robotName, controller.stop());
                            for (int index = 0; index < 2; index++) {
                                TimeUnit.SECONDS.sleep(1);
                                communicator.send(robotName, controller.stop());
                            }
                            return;
                        }
                    }

                    double degree = getBearing();
                    double distance = getDistance(currentPosition);

                    LOGGER.info("Kat i odleglosc pojazdu od obecnego celu: " + degree + ", " + distance);
                    communicator.send(robotName, controller.goToNextPoint(degree, distance));
                } else {
                    communicator.send(robotName, controller.stop());
                }
                TimeUnit.SECONDS.sleep(1);
            }
        }
        catch (InterruptedException e) {
            e.printStackTrace();
            isConnected.set(false);
        }
    }

    private void checkConnection() {
        try {
            while (!isConnected.get()) {
                String receivedMessage = communicator.getMessageFromVehicle(robotName);

                currentPosition = controller.getVehiclePosition(receivedMessage);
                while (currentPosition.latitude == 0.0 && currentPosition.longitude == 0.0) {
                    LOGGER.finest("Oczekiwanie na ustalenie pozycji pojazdu: " + robotName + " " + currentPosition.latitude + ", " + currentPosition.longitude);
                    TimeUnit.SECONDS.sleep(1);

                    receivedMessage = communicator.getMessageFromVehicle(robotName);
                    currentPosition = controller.getVehiclePosition(receivedMessage);
                }

                LOGGER.info("Pozycja " + robotName + " zostanie naniesiona na mapę " + currentPosition.latitude + " " + currentPosition.longitude);
                isConnected.set(true);
            }
        }
        catch (InterruptedException e) {
            e.printStackTrace();
            isConnected.set(false);
        }
    }

    /**
     * Oblicza kąt pomiędzy kierunkiem w jakim ustawiony jest pojazd a aktualnym celem
     * <p>
     * http://www.movable-type.co.uk/scripts/latlong.html
     */
    private double getBearing() {
        double absoluteAngle = calculateBearing(previousPosition, currentPosition);
        double relativeAngle = calculateBearing(currentPosition, currentGoal);
        double bearing = relativeAngle - absoluteAngle;
        bearing = Math.round(bearing);

        bearing = bearing % 360;

        if(bearing < 0){
            return 360 + bearing;
        } else {
            return bearing;
        }
    }

    private double calculateBearing(VehiclePosition from, VehiclePosition to) {
        double x = (Math.cos(from.latitude) * Math.sin(to.latitude))
                - (Math.sin(from.latitude) * Math.cos(to.latitude)
                * Math.cos(to.longitude - from.longitude));

        double y = Math.sin(to.longitude - from.longitude) * Math.cos(to.latitude);

        return (Math.toDegrees(Math.atan2(y, x))) % 360;
    }

    /**
     * Oblicza odległość pomiędzy pojazdem a aktualnym celem
     * <p>
     * http://www.movable-type.co.uk/scripts/latlong.html
     */
    private double getDistance(VehiclePosition position) {
        int R = 6371;

        double latitudeDistance = Math.toRadians(currentGoal.latitude - position.latitude);
        double longitudeDistance = Math.toRadians(currentGoal.longitude - position.longitude);
        double a = Math.sin(latitudeDistance / 2.0) * Math.sin(latitudeDistance / 2.0)
                + Math.cos(Math.toRadians(position.latitude)) * Math.cos(Math.toRadians(currentGoal.latitude))
                * Math.sin(longitudeDistance / 2.0) * Math.sin(longitudeDistance / 2.0);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = R * c * 1000; //w metrach

        return Math.round(d);
    }

    /**
     * Funkcja wyznaczająca następny cel na podstawie zaplanowanej trasy
     *
     * @return boolean
     */
    public void setNextGoal() {
        for (Checkpoint el : checkpoints) {
            if (!el.isDone()) {
                currentGoal = new VehiclePosition(el.getLatitude(), el.getLongitude());
                LOGGER.info("SetNextGoal - ustawiono nowy cel dla pojazdu");
                return;
            }
        }
    }

    /**
     * Funkcja sprawdzająca czy aktualna pozycja robota pokrywa się z aktualnym celem. Jeśli tak usuwa cel z listy checkpoints
     *
     * @param currentPosition
     * @return boolran
     */
    private boolean isGoalAchieved(VehiclePosition currentPosition) {
        int tmpDistance = (int) getDistance(currentPosition);
        boolean isAchieved = tmpDistance <= GPS_ACCURACY;
        LOGGER.finest("Obecny cel pojazdu nie został jeszczez spełniony");
        if (isAchieved) {
            LOGGER.info("Obecny cel pojazdu został spełniony");
            for (Checkpoint el : checkpoints) {
                if (!el.isDone()) {
                    el.setDone();
                    return true;
                }
            }

        }
        return isAchieved;
    }

    /**
     * Funkcja sprawdza czy istnieje następny cel
     *
     * @return boolean
     */
    private boolean hasNext() {
        for (Checkpoint el : checkpoints) {
            if (!el.isDone())
                return true;
        }

        return false;
    }

    public ArrayList<VehiclePosition> getListOfReceivedVehiclePositions() {
        return listOfReceivedVehiclePositions;
    }

    public void runNavigationProcess(ArrayList<Checkpoint> checkpointList) {
        checkpoints = checkpointList;
        runNavigation.set(true);
        listOfReceivedVehiclePositions.clear();
        setNextGoal();
    }
}
