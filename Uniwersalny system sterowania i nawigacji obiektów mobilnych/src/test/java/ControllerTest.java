import com.mateusz.nawigacja.serwer.Interfaces.VehicleInterface;
import com.mateusz.nawigacja.serwer.Navigation.VehiclePosition;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Objects;

public class ControllerTest {
    //Uzupełnić
    private static VehicleInterface controller;

    private static final String ROBOT_NAME = "";
    
    private static final String RECEIVED_MESSAGE = "";
    private static final double EXPECTED_LATITUDE = 0.0;
    private static final double EXPECTED_LONGITUDE = 0.0;
    
    private static final double DEGREE = 0.0;
    private static final double DISTANCE = 0.0;
    private static final String EXPECTED_RESULT = "";

    private static final String EXPECTED_STOP_COMMAND = "";

    @BeforeAll
    public static void initTest() {
        boolean readingError = false;
        File directory = new File("drivers/");
        for (File jar : Objects.requireNonNull(directory.listFiles()))
            try {
                ClassLoader loader = URLClassLoader.newInstance(
                        new URL[]{jar.toURI().toURL()}
                );

                Class controllerClass = Class.forName("Controller", true, loader);
                controller = (VehicleInterface) controllerClass.newInstance();

            } catch (ClassNotFoundException e) {
                readingError = true;
                e.printStackTrace();
            } catch (MalformedURLException | IllegalAccessException | InstantiationException e) {
                e.printStackTrace();
            }
        if (readingError) {
            Assertions.fail("Nie udało się pomyślnie wczytać wszystkich sterowników pojazdów z katalogu drivers");
        }
    }

    @Test
    public void testRobotName() {
        String assertName = controller.getRobotName();
        Assertions.assertTrue(assertName.equals(ROBOT_NAME));
    }

    @Test
    public void testParsingMessage() {
        VehiclePosition position = controller.getVehiclePosition(RECEIVED_MESSAGE);
        Assertions.assertEquals(EXPECTED_LATITUDE, position.latitude);
        Assertions.assertEquals(EXPECTED_LONGITUDE, position.longitude);
    }
    
    @Test
    public void testGoToNextPointFunctionLoop() {
        String preparedCommand = controller.goToNextPoint(DEGREE,DISTANCE);
        Assertions.assertEquals(EXPECTED_RESULT, preparedCommand);
       
    }

    @Test
    public void testStopFunction() {
        String preparedStopCommand = controller.stop();
        Assertions.assertEquals(EXPECTED_STOP_COMMAND, preparedStopCommand);
    }
    
}
