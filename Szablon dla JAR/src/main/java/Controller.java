import com.mateusz.nawigacja.serwer.Interfaces.VehicleInterface;
import com.mateusz.nawigacja.serwer.Navigation.VehiclePosition;

public class Controller implements VehicleInterface {

    private static String ROBOT_NAME = "";

    @Override
    public String getRobotName() {
        return ROBOT_NAME;
    }

    @Override
    public VehiclePosition getVehiclePosition(String messageFromVehicle) {
      return new VehiclePosition(4,4);
    }

    @Override
    public String goToNextPoint(double degree, double distance) {
        return null;
    }

    @Override
    public String stop() {
        return null;
    }

}
